/*
 * Copyright (C) 2017 Moez Bhatti <moez.bhatti@gmail.com>
 *
 * This file is part of QKSMS.
 *
 * QKSMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QKSMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QKSMS.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.madfox.madsms.injection

import com.madfox.madsms.common.QKApplication
import com.madfox.madsms.common.QkDialog
import com.madfox.madsms.common.util.QkChooserTargetService
import com.madfox.madsms.common.widget.AvatarView
import com.madfox.madsms.common.widget.PagerTitleView
import com.madfox.madsms.common.widget.PreferenceView
import com.madfox.madsms.common.widget.QkEditText
import com.madfox.madsms.common.widget.QkSwitch
import com.madfox.madsms.common.widget.QkTextView
import com.madfox.madsms.common.widget.RadioPreferenceView
import com.madfox.madsms.feature.backup.BackupController
import com.madfox.madsms.feature.blocking.BlockingController
import com.madfox.madsms.feature.blocking.manager.BlockingManagerController
import com.madfox.madsms.feature.blocking.messages.BlockedMessagesController
import com.madfox.madsms.feature.blocking.numbers.BlockedNumbersController
import com.madfox.madsms.feature.compose.editing.DetailedChipView
import com.madfox.madsms.feature.conversationinfo.injection.ConversationInfoComponent
import com.madfox.madsms.feature.settings.SettingsController
import com.madfox.madsms.feature.settings.about.AboutController
import com.madfox.madsms.feature.settings.swipe.SwipeActionsController
import com.madfox.madsms.feature.themepicker.injection.ThemePickerComponent
import com.madfox.madsms.feature.widget.WidgetAdapter
import com.madfox.madsms.injection.android.ActivityBuilderModule
import com.madfox.madsms.injection.android.BroadcastReceiverBuilderModule
import com.madfox.madsms.injection.android.ServiceBuilderModule
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    ActivityBuilderModule::class,
    BroadcastReceiverBuilderModule::class,
    ServiceBuilderModule::class])
interface AppComponent {

    fun conversationInfoBuilder(): ConversationInfoComponent.Builder
    fun themePickerBuilder(): ThemePickerComponent.Builder

    fun inject(application: QKApplication)

    fun inject(controller: AboutController)
    fun inject(controller: BackupController)
    fun inject(controller: BlockedMessagesController)
    fun inject(controller: BlockedNumbersController)
    fun inject(controller: BlockingController)
    fun inject(controller: BlockingManagerController)
    fun inject(controller: SettingsController)
    fun inject(controller: SwipeActionsController)

    fun inject(dialog: QkDialog)

    fun inject(service: WidgetAdapter)

    /**
     * This can't use AndroidInjection, or else it will crash on pre-marshmallow devices
     */
    fun inject(service: QkChooserTargetService)

    fun inject(view: AvatarView)
    fun inject(view: DetailedChipView)
    fun inject(view: PagerTitleView)
    fun inject(view: PreferenceView)
    fun inject(view: RadioPreferenceView)
    fun inject(view: QkEditText)
    fun inject(view: QkSwitch)
    fun inject(view: QkTextView)

}
