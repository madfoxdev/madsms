package com.madfox.madsms.feature.wishes

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.madfox.madsms.R
import com.madfox.madsms.manager.AnalyticsManager


class smsCategories : Fragment(), wishCatAdapter.onMaincatClick {
    private lateinit var wishExchange: WishExchange
    private lateinit var analyticsManager: AnalyticsManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val hashMap: HashMap<Int, String> = linkedMapOf<Int, String>()
        hashMap.put(R.drawable.good_morning, "Good Morning Wishes")
        hashMap.put(R.drawable.good_night, "Good Night Wishes")
        hashMap.put(R.drawable.birthday, "Birthday Wishes")
        hashMap.put(R.drawable.engagement_anniverseary, "Engagement Anniversary")
        hashMap.put(R.drawable.proposal, "Propose Messages")
        hashMap.put(R.drawable.new_year, "New Year Wishes")
        hashMap.put(R.drawable.fathers_day, "Father's Day Wishes")
        hashMap.put(R.drawable.wedding_anniverseary, "Wedding Anniversary Wishes")
        hashMap.put(R.drawable.easter, "Easter Day Wishes")
        hashMap.put(R.drawable.graduation, "Graduation Day Wishes")
        hashMap.put(R.drawable.get_well_soon, "Get Well Soon Messages")
        hashMap.put(R.drawable.valentines_day, "Valentine's Day Wishes")
        hashMap.put(R.drawable.appology, "Appology Messages")
        hashMap.put(R.drawable.christmas, "Christmas Wishes")
        hashMap.put(R.drawable.friendship, "Best Friend Messages")
        hashMap.put(R.drawable.mothers_day, "Mother's Day Wishes")
        hashMap.put(R.drawable.engagement, "Engagement Wishes")
        hashMap.put(R.drawable.love_anniverseary, "Love Anniversary")
        hashMap.put(R.drawable.wedding, "Wedding day Wishes")
        hashMap.put(R.drawable.romantic_wishes, "Romantic Messages")
        hashMap.put(R.drawable.work_anniverseary, "Work Anniversary Wishes")

        val view = inflater.inflate(R.layout.fragment_sms_categories, container, false)
        val recyclerview = view.findViewById<RecyclerView>(R.id.recyclerSmsCats)
        analyticsManager = activity as AnalyticsManager
        analyticsManager.trackScreens("sms categories")
         wishExchange = activity as WishExchange
        // this creates a vertical layout Manager
        recyclerview.layoutManager = LinearLayoutManager(context)
        val data = ArrayList<wishCats>()
        for (entry in hashMap.entries){
            data.add(wishCats(entry.key, entry.value))
        }
        val adapter = wishCatAdapter(data,this)

        

        recyclerview.adapter = adapter
        return view
    }

    override fun onItemClick(catName: String, catImage: Int) {
        analyticsManager.trackActions("sms categories","${catName} clicked")
        wishExchange.sendCatInfo(catName, catImage)
    }

}