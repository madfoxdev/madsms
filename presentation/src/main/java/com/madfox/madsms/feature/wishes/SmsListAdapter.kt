package com.madfox.madsms.feature.wishes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.madfox.madsms.R

class SmsListAdapter(private val wishlst: ArrayList<Wishes>, private val listner: OnItemClickListener_wishes, private val progressBar: ProgressBar) :
    RecyclerView.Adapter<SmsListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SmsListAdapter.ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.single_wish, parent, false)

        return ViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: SmsListAdapter.ViewHolder, position: Int) {
        val currentitem: Wishes = wishlst[0]
        holder.txtCat.text  = currentitem.cat
        holder.txtWish.text = currentitem.wish?.get(position) ?: "0"
        holder.imageView.setImageResource(currentitem.image)
        progressBar.visibility = View.GONE
//        holder.progressBar.visibility =View.GONE

    }



    override fun getItemCount(): Int {
        val subwishes: Wishes = wishlst[0]
        val size: Int = subwishes.wish?.size ?: 0
        return size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
//        val lay: ConstraintLayout = view.findViewById(R.id.maincat_layout)
        val txtCat: TextView = view.findViewById(R.id.txtcat)
        val txtWish: TextView = view.findViewById(R.id.txt_wish_title)
        val imageView: ImageView = view.findViewById(R.id.catImage)
        val imgShare:ImageView = view.findViewById(R.id.imgShare)
        val imgSMS:ImageView = view.findViewById(R.id.imgSMS)
        val imgSchedule:ImageView = view.findViewById(R.id.icon_schedule)


        init {
            itemView.setOnClickListener(this)
            imgSchedule.setOnClickListener(this)
            imgShare.setOnClickListener(this)
            imgSMS.setOnClickListener(this)
        }


        override fun onClick(v: View?) {
            val wish: String = wishlst[0].wish?.get(adapterPosition).toString()
           if (v != null) {
                       when(v.id){
                           R.id.imgShare -> share(wish,"share",adapterPosition)
                           R.id.imgSMS -> share(wish,"sms",adapterPosition)
                           R.id.icon_schedule -> share(wish,"schedule",adapterPosition)
                       }
                   }

        }

        private fun share(wish: String, s: String, adapterPosition: Int) {
            listner.onItemClick(wish,s,adapterPosition)
        }
    }

    interface OnItemClickListener_wishes {
        fun onItemClick(wish: String, btnName:String, position: Int)
    }

}