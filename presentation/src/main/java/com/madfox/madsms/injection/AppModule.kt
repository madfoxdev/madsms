/*
 * Copyright (C) 2017 Moez Bhatti <moez.bhatti@gmail.com>
 *
 * This file is part of QKSMS.
 *
 * QKSMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QKSMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QKSMS.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.madfox.madsms.injection

import android.app.Application
import android.content.ContentResolver
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.lifecycle.ViewModelProvider
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.madfox.madsms.blocking.BlockingClient
import com.madfox.madsms.blocking.BlockingManager
import com.madfox.madsms.common.ViewModelFactory
import com.madfox.madsms.common.util.BillingManagerImpl
import com.madfox.madsms.common.util.NotificationManagerImpl
import com.madfox.madsms.common.util.ShortcutManagerImpl
import com.madfox.madsms.feature.conversationinfo.injection.ConversationInfoComponent
import com.madfox.madsms.feature.themepicker.injection.ThemePickerComponent
import com.madfox.madsms.listener.ContactAddedListener
import com.madfox.madsms.listener.ContactAddedListenerImpl
import com.madfox.madsms.manager.ActiveConversationManager
import com.madfox.madsms.manager.ActiveConversationManagerImpl
import com.madfox.madsms.manager.AlarmManager
import com.madfox.madsms.manager.AlarmManagerImpl
import com.madfox.madsms.manager.AnalyticsManager
import com.madfox.madsms.manager.AnalyticsManagerImpl
import com.madfox.madsms.manager.BillingManager
import com.madfox.madsms.manager.ChangelogManager
import com.madfox.madsms.manager.ChangelogManagerImpl
import com.madfox.madsms.manager.KeyManager
import com.madfox.madsms.manager.KeyManagerImpl
import com.madfox.madsms.manager.NotificationManager
import com.madfox.madsms.manager.PermissionManager
import com.madfox.madsms.manager.PermissionManagerImpl
import com.madfox.madsms.manager.RatingManager
import com.madfox.madsms.manager.ReferralManager
import com.madfox.madsms.manager.ReferralManagerImpl
import com.madfox.madsms.manager.ShortcutManager
import com.madfox.madsms.manager.WidgetManager
import com.madfox.madsms.manager.WidgetManagerImpl
import com.madfox.madsms.mapper.CursorToContact
import com.madfox.madsms.mapper.CursorToContactGroup
import com.madfox.madsms.mapper.CursorToContactGroupImpl
import com.madfox.madsms.mapper.CursorToContactGroupMember
import com.madfox.madsms.mapper.CursorToContactGroupMemberImpl
import com.madfox.madsms.mapper.CursorToContactImpl
import com.madfox.madsms.mapper.CursorToConversation
import com.madfox.madsms.mapper.CursorToConversationImpl
import com.madfox.madsms.mapper.CursorToMessage
import com.madfox.madsms.mapper.CursorToMessageImpl
import com.madfox.madsms.mapper.CursorToPart
import com.madfox.madsms.mapper.CursorToPartImpl
import com.madfox.madsms.mapper.CursorToRecipient
import com.madfox.madsms.mapper.CursorToRecipientImpl
import com.madfox.madsms.mapper.RatingManagerImpl
import com.madfox.madsms.repository.BackupRepository
import com.madfox.madsms.repository.BackupRepositoryImpl
import com.madfox.madsms.repository.BlockingRepository
import com.madfox.madsms.repository.BlockingRepositoryImpl
import com.madfox.madsms.repository.ContactRepository
import com.madfox.madsms.repository.ContactRepositoryImpl
import com.madfox.madsms.repository.ConversationRepository
import com.madfox.madsms.repository.ConversationRepositoryImpl
import com.madfox.madsms.repository.MessageRepository
import com.madfox.madsms.repository.MessageRepositoryImpl
import com.madfox.madsms.repository.ScheduledMessageRepository
import com.madfox.madsms.repository.ScheduledMessageRepositoryImpl
import com.madfox.madsms.repository.SyncRepository
import com.madfox.madsms.repository.SyncRepositoryImpl
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(subcomponents = [
    ConversationInfoComponent::class,
    ThemePickerComponent::class])
class AppModule(private var application: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context = application

    @Provides
    fun provideContentResolver(context: Context): ContentResolver = context.contentResolver

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @Provides
    @Singleton
    fun provideRxPreferences(preferences: SharedPreferences): RxSharedPreferences {
        return RxSharedPreferences.create(preferences)
    }

    @Provides
    @Singleton
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
    }

    @Provides
    fun provideViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory = factory

    // Listener

    @Provides
    fun provideContactAddedListener(listener: ContactAddedListenerImpl): ContactAddedListener = listener

    // Manager

    @Provides
    fun provideBillingManager(manager: BillingManagerImpl): BillingManager = manager

    @Provides
    fun provideActiveConversationManager(manager: ActiveConversationManagerImpl): ActiveConversationManager = manager

    @Provides
    fun provideAlarmManager(manager: AlarmManagerImpl): AlarmManager = manager

    @Provides
    fun provideAnalyticsManager(manager: AnalyticsManagerImpl): AnalyticsManager = manager

    @Provides
    fun blockingClient(manager: BlockingManager): BlockingClient = manager

    @Provides
    fun changelogManager(manager: ChangelogManagerImpl): ChangelogManager = manager

    @Provides
    fun provideKeyManager(manager: KeyManagerImpl): KeyManager = manager

    @Provides
    fun provideNotificationsManager(manager: NotificationManagerImpl): NotificationManager = manager

    @Provides
    fun providePermissionsManager(manager: PermissionManagerImpl): PermissionManager = manager

    @Provides
    fun provideRatingManager(manager: RatingManagerImpl): RatingManager = manager

    @Provides
    fun provideShortcutManager(manager: ShortcutManagerImpl): ShortcutManager = manager

    @Provides
    fun provideReferralManager(manager: ReferralManagerImpl): ReferralManager = manager

    @Provides
    fun provideWidgetManager(manager: WidgetManagerImpl): WidgetManager = manager

    // Mapper

    @Provides
    fun provideCursorToContact(mapper: CursorToContactImpl): CursorToContact = mapper

    @Provides
    fun provideCursorToContactGroup(mapper: CursorToContactGroupImpl): CursorToContactGroup = mapper

    @Provides
    fun provideCursorToContactGroupMember(mapper: CursorToContactGroupMemberImpl): CursorToContactGroupMember = mapper

    @Provides
    fun provideCursorToConversation(mapper: CursorToConversationImpl): CursorToConversation = mapper

    @Provides
    fun provideCursorToMessage(mapper: CursorToMessageImpl): CursorToMessage = mapper

    @Provides
    fun provideCursorToPart(mapper: CursorToPartImpl): CursorToPart = mapper

    @Provides
    fun provideCursorToRecipient(mapper: CursorToRecipientImpl): CursorToRecipient = mapper

    // Repository

    @Provides
    fun provideBackupRepository(repository: BackupRepositoryImpl): BackupRepository = repository

    @Provides
    fun provideBlockingRepository(repository: BlockingRepositoryImpl): BlockingRepository = repository

    @Provides
    fun provideContactRepository(repository: ContactRepositoryImpl): ContactRepository = repository

    @Provides
    fun provideConversationRepository(repository: ConversationRepositoryImpl): ConversationRepository = repository

    @Provides
    fun provideMessageRepository(repository: MessageRepositoryImpl): MessageRepository = repository

    @Provides
    fun provideScheduledMessagesRepository(repository: ScheduledMessageRepositoryImpl): ScheduledMessageRepository = repository

    @Provides
    fun provideSyncRepository(repository: SyncRepositoryImpl): SyncRepository = repository

}