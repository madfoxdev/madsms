package com.madfox.madsms.feature.wishes

import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.madfox.madsms.R
import kotlinx.android.synthetic.main.gallery_image_page.*
import kotlinx.android.synthetic.main.wish_item.*

class wishCatAdapter(private val mList: List<wishCats>, private var clickListener: onMaincatClick) :
    RecyclerView.Adapter<wishCatAdapter.ViewHolder>() {


    inner class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView), View.OnClickListener {
        val imgProfile: ImageView = itemView.findViewById(R.id.imgProfileImage)
        val txtCats: TextView = itemView.findViewById(R.id.txtCatName)
        val layoutCat:ConstraintLayout = itemView.findViewById(R.id.layoutWishcat)
        init {
            layoutCat.setOnClickListener (this)
        }

        override fun onClick(v: View?) {
            val cat_name:String = mList[adapterPosition].text
            val catImg:Int = mList[adapterPosition].image
            clickListener.onItemClick(cat_name,catImg)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.wish_item, parent, false)

        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ItemsViewModel = mList[position]
        holder.imgProfile.setImageResource(ItemsViewModel.image)
        holder.txtCats.text = ItemsViewModel.text

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    interface  onMaincatClick {
        fun onItemClick(catName:String, catImage:Int)
    }
}