package com.madfox.madsms.manager

interface ReferralManager {

    suspend fun trackReferrer()

}
