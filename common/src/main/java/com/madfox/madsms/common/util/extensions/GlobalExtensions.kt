package com.madfox.madsms.common.util.extensions

fun now(): Long {
    return System.currentTimeMillis()
}
