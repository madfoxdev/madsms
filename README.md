##   MAD SMS

MAD SMS is  a modern sms manager that allow users to schedule / send sms from their android phones 

This project is a modified version of QKSMS  which released under GPLv3 - https://github.com/moezbhatti/qksms




## License

MAD SMS  is released under the **The GNU General Public License v3.0 (GPLv3)**, which can be found in the `LICENSE` file in the root of this project.

## Changelogs 
- changed contact info
- added CHANGELOG.md
- added firebase analytics
- adding firebase services


