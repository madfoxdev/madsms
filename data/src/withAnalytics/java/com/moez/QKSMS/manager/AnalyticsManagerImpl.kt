/*
 * Copyright (C) 2017 Moez Bhatti <moez.bhatti@gmail.com>
 *
 * This file is part of QKSMS.
 *
 * QKSMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QKSMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QKSMS.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.madfox.madsms.manager

import android.content.Context
import android.os.Bundle
import androidx.core.os.bundleOf
import com.google.firebase.analytics.FirebaseAnalytics

import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AnalyticsManagerImpl @Inject constructor(context: Context) : AnalyticsManager {
    private lateinit var analytics: FirebaseAnalytics


    init {
        analytics = FirebaseAnalytics.getInstance(context)
    }

    override fun track(event: String, vararg properties: Pair<String, Any>) {
        val  bundle =  Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event)
        analytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM,bundle)

        // Todo-: add analytics


        }

    override fun trackScreens(screen: String) {
//        TODO("Not yet implemented")
    }

    override fun trackActions(screen: String, action: String) {
//        TODO("Not yet implemented")
    }


    override fun setUserProperty(key: String, value: Any) {

        // Todo-: add analytics
      /*  Timber.v("$key: $value")

        // Set the value in Amplitude
        val identify = Identify()
        when (value) {
            is Boolean -> identify.set(key, value)
            is BooleanArray -> identify.set(key, value)
            is Double -> identify.set(key, value)
            is DoubleArray -> identify.set(key, value)
            is Float -> identify.set(key, value)
            is FloatArray -> identify.set(key, value)
            is Int -> identify.set(key, value)
            is IntArray -> identify.set(key, value)
            is Long -> identify.set(key, value)
            is LongArray -> identify.set(key, value)
            is String -> identify.set(key, value)
            is JSONArray -> identify.set(key, value)
            is JSONObject -> identify.set(key, value)
            else -> Timber.e("Value of type ${value::class.java} not supported")
        }
        amplitude.identify(identify)*/
    }

}
