package com.madfox.madsms.feature.wishes

data class Wishes(val image: Int, val cat: String, val wish:List<String>? =null)
