package com.madfox.madsms.feature.wishes

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.madfox.madsms.R
import com.madfox.madsms.manager.AnalyticsManager

class WishesHome : AppCompatActivity(), WishExchange, AnalyticsManager, loadWishcats {
    private lateinit var analytics: FirebaseAnalytics
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wishes_home)
        analytics = Firebase.analytics
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().replace(R.id.fragment_main, smsCategories())
                .commit()
        }

    }

    override fun sendCatInfo(catName: String, catImage: Int) {

        val bundle = Bundle()
        bundle.putString("catName",catName)
        bundle.putInt("catImage",catImage)
        val fragment = WishList()
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_main,fragment)
            .addToBackStack("sms list")
            .commit()


    }

    override fun track(event: String, vararg properties: Pair<String, Any>) {

    }

    override fun trackScreens(screen: String) {
        analytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, screen)
        }
    }

    override fun trackActions(screen: String, action: String) {
        analytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
            param(FirebaseAnalytics.Param.ITEM_NAME, action)
            param(FirebaseAnalytics.Param.SCREEN_NAME, screen)
        }
    }

    override fun setUserProperty(key: String, value: Any) {
    }

    override fun goToWishcats() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_main,smsCategories())
            .addToBackStack("sms cats")
            .commit()
    }


}