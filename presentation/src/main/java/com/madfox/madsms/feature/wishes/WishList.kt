package com.madfox.madsms.feature.wishes

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Source
import com.madfox.madsms.R
import com.madfox.madsms.manager.AnalyticsManager


class WishList : Fragment(), SmsListAdapter.OnItemClickListener_wishes {
    private var catName: String = "Good Morning Wishes"
    private var catImage: Int = 0
    private lateinit var db: FirebaseFirestore
    private var userArrayList: ArrayList<Wishes> = ArrayList()
    private lateinit var userRecyclerview: RecyclerView
    private lateinit var analyticsManager: AnalyticsManager
    private lateinit var loadwishcats: loadWishcats
    private lateinit var progressBar: ProgressBar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            catName = requireArguments().getString("catName").toString()
            catImage = requireArguments().getInt("catImage")

        }
        loadwishcats = activity as loadWishcats
        analyticsManager = activity as AnalyticsManager
        analyticsManager.trackScreens(catName)
        getUserDataOnce()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_sms_item, container, false)
        userRecyclerview = view.findViewById(R.id.recylerWishList)
        userRecyclerview.layoutManager = LinearLayoutManager(context)
        progressBar = view.findViewById(R.id.progressBar)
//        Log.d("catName",requireArguments().getString("catName").toString() )
        return view
    }

    private fun getUserDataOnce() {
        val cache: Source = Source.CACHE
        val server: Source = Source.SERVER
        db = FirebaseFirestore.getInstance()
//        val docRef = db.collection("main_categories")
        catName.let { it1 ->
            db.collection("wishes").document(it1)
                .get(cache)
                .addOnSuccessListener { result ->

                    userArrayList.add(
                        Wishes(
                            catImage,
                            catName,
                            result.data!!["sms"] as List<String>?

                        )
                    )


                    userRecyclerview.adapter = SmsListAdapter(userArrayList, this,progressBar)
//                    progressbar.visibility = View.GONE
//                    userRecyclerview.visibility = View.VISIBLE

                }
        }
            .addOnFailureListener { exception ->
                //FirebaseFirestoreException firestoreException = (FirebaseFirestoreException)exception;
                val firestoreExpeption: FirebaseFirestoreException =
                    exception as FirebaseFirestoreException
                if (firestoreExpeption.code.name == "UNAVAILABLE") {
                    catName.let { it1 ->
                        db.collection("wishes").document(it1)
                            .get(server)
                            .addOnSuccessListener { result1 ->
                                userArrayList.add(
                                    Wishes(
                                        catImage,
                                        catName,
                                        result1.data!!["sms"] as List<String>?

                                    )
                                )
                                userRecyclerview.adapter =
                                    SmsListAdapter(userArrayList, this,progressBar)
//                                progressbar.visibility = View.GONE
//                                userRecyclerview.visibility = View.VISIBLE

                            }
                    }
                        .addOnFailureListener {
                            noInternetDialog()
//                            val internetStatus = context?.let { it1 -> isInternetConnected(it1) }

//                                checkInternet = activity as checkInternet
//                                checkInternet.internetChekDialog("Plase Check Your Internet Connection")


                        }
                } else {
                    noInternetDialog()
//                    val internetStatus = context?.let { it1 -> isInternetConnected(it1) }
//                                checkInternet = activity as checkInternet
//                                checkInternet.internetChekDialog("Plase Check Your Internet Connection")

                }

            }
    }

    fun isInternetConnected(getApplicationContext: Context): Boolean {
        var status = false

        val cm: ConnectivityManager =
            getApplicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (cm.activeNetwork != null && cm.getNetworkCapabilities(cm.activeNetwork) != null) {
                // connected to the internet
                status = true
            }

        } else {
            if (cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnectedOrConnecting) {
                // connected to the internet
                status = true
            }
        }

        return status
    }

    override fun onItemClick(wish: String, btnName: String, position: Int) {

        when (btnName) {
            "share" -> share(wish)
            "sms" -> sendSMS(wish)
            "schedule" -> scheduleSMS(wish)
        }

    }

    private fun sendSMS(wishSMS: String) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("smsto:")
            putExtra("sms_body", wishSMS)
            setPackage("com.madfox.madsms")
        }
        try {
            analyticsManager.trackActions(catName, "Send SMS")
            startActivity(intent)
        } catch (e: Exception) {
//            madSMSInstall("Schedule SMS", "You need MadSMS App to schedule messages. Would you like to install it now ?", requireActivity())
//            val alert = ViewDialog()
//            alert.showInstallMadSMS(activity)
        }
    }

    private fun scheduleSMS(wishSMS: String) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("smsto:")
            putExtra("sms_body", wishSMS)
            putExtra("scheduled", true)
            setPackage("com.madfox.madsms")
        }
        try {
            analyticsManager.trackActions(catName, "Schedule SMS")
            startActivity(intent)
        } catch (e: Exception) {
//            madSMSInstall("Schedule SMS", "You need MadSMS App to schedule messages. Would you like to install it now ?", requireActivity())
//            val alert = ViewDialog()
//            alert.showInstallMadSMS(activity)
        }

    }

    private fun share(wishSMS: String) {
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.putExtra(Intent.EXTRA_TEXT, wishSMS)
        intent.type = "text/plain"
        analyticsManager.trackActions(catName, "Share Text")
        startActivity(Intent.createChooser(intent, "Share To:"))

    }

    fun noInternetDialog() {
        activity?.let {
            val builder = AlertDialog.Builder(it)

            builder.apply {
                setMessage("Download Failed!. Please Check your Internet Connection")
                setPositiveButton(
                    "OK"
                ) { dialog, id ->
                    loadwishcats.goToWishcats()
                }

            }


            builder.create()
        }?.show()

    }


}